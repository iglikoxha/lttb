import h5py
import matplotlib.pyplot as plt
import numpy as np 

def _areas_of_triangles(a, bs, c):
    bs_minus_a = bs - a
    a_minus_bs = a - bs
    return 0.5 * abs((a[0] - c[0]) * (bs_minus_a[:, 1])
                     - (a_minus_bs[:, 0]) * (c[1] - a[1]))


def downsample(data, n_out):
    # Validate input
    if data.shape[1] != 2:
        raise ValueError('data should have 2 columns')

    data = data[data[:, 0].argsort()]

    if n_out > data.shape[0]:
        raise ValueError('n_out must be <= number of rows in data')

    if n_out == data.shape[0]:
        return data

    if n_out < 3:
        raise ValueError('Can only downsample to a minimum of 3 points')

    # Split data into bins
    n_bins = n_out - 2
    data_bins = np.array_split(data[1: len(data) - 1], n_bins)

    # Prepare output array
    # First and last points are the same as in the input.
    out = np.zeros((n_out, 2))
    out[0] = data[0]
    out[len(out) - 1] = data[len(data) - 1]

    # Largest Triangle Three Buckets (LTTB):
    # In each bin, find the point that makes the largest triangle
    # with the point saved in the previous bin
    # and the centroid of the points in the next bin.
    for i in range(len(data_bins)):
        this_bin = data_bins[i]

        if i < n_bins - 1:
            next_bin = data_bins[i + 1]
        else:
            next_bin = data[len(data) - 1:]

        a = out[i]
        bs = this_bin
        c = next_bin.mean(axis=0)

        areas = _areas_of_triangles(a, bs, c)

        out[i + 1] = bs[np.argmax(areas)]

    return out

f = h5py.File('114162_FKZP_R4_3_100_RT.H5')

dset = f.get('114162_FKZP_R4_3_100_RT_1').get('KrKMG')[:]

data = np.array([range(len(dset)), dset]).T
arr1, arr2 = np.split(data, 2, axis=1)

small_data = downsample(data, n_out=1000)
arr3, arr4 = np.split(small_data, 2, axis=1)

slice_data = dset[::500]

mean_data = np.mean(dset.reshape(-1, 500), 1)

print("Original points:", arr2.shape)
print("LTTB data points: ", arr4.shape)
print("Sliced data points: ", slice_data.shape)
print("Meaned/Averaged data points: ", mean_data.shape)

#ORIGINAL
plot1 = plt.figure('Original')
plt.plot(arr1, arr2)

#Largest-Triangle-Three-Buckets
plot2 = plt.figure('Largest-Triangle-Three-Buckets')
plt.plot(arr3, arr4)

#Slicing/Thinning The Array
plot3 = plt.figure('Slicing the data')
plt.plot(slice_data)

#Mean/Averaging Downsampling
plot4 = plt.figure('Mean/Averaging the data')
plt.plot(mean_data)

plt.show()
